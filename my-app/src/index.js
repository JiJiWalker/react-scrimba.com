// TODOApp
import React from "react"
import ReactDOM from "react-dom"
import App from "./components/App"
import "./index.css"


ReactDOM.render(<App/>, document.getElementById("root"))


/**** 7 ***
import React from "react"
import ReactDOM from "react-dom"
import AppPractice5 from "./components/AppPractice5"
import "./index.css"

// ReactDOM.render(<AppPractice5/>, document.getElementById("root"))
*/

/**** 8 ***
import React from "react"
import ReactDOM from "react-dom"
import Temp from "./files/Temp";

// normaln function JS
function MyApp() {
    return (
        <h1>Some text</h1>
    )
}

// arrow function JS
const Temp = () => <h1>Some text</h1>

ReactDOM.render(<AppPractice5/>, document.getElementById("root"))
*/

/**** 9 ***
import React from "react"
import ReactDOM from "react-dom"

function App() {
    const firstName = "Jakub";
    const lastName = "Bukaj";

    return (
        <h1>Hello {firstName + " " + lastName}</h1>
    )
}

ReactDOM.render(<App/>, document.getElementById("root"))
*/

/**** 10 ***
import React from "react"
import ReactDOM from "react-dom"

function App() {
    const date = new Date();
    const hours = date.getHours();
    let timeOfDay;
    // *style = we can move {{color: "", backColor: ""}} to variable
    const style = {
        fontSize: 30,
        color: "#FF8C00"
    }

    if (hours < 12) {
        timeOfDay = "morning";
        style.backgroundColor = "red";
    } else if (hours >= 12 && hours <17) {
        timeOfDay = "afternoon";
        style.backgroundColor = "yellow";
    } else {
        timeOfDay = "night";
        style.backgroundColor = "green";
    }

    

    return (
        // style should be an object
        // first {} is object declar, second {} is escape from JSX to JS
        // every tag with something separeting like background-color should be write without is with camelCase
        // *style
        <h1 style={style}>Good {timeOfDay}</h1>
    )
}

// arrow func
// const myFunc = (param1, param2) => doSomething(param1, param2)

// AppArrow => {
//     const date = new Date();
//     const hours = date.getHours();
//     let timeOfDay;

//     if (hourrse < 12) {
//         timeOfDay = "morning";
//     } else if (hours >= 12 && hours <17) {
//         timeOfDay = "afternoon";
//     } else {
//         timeOfDay = "night";
//     }
//     return <h1>Good {timeOfDay}</h1>
// }


ReactDOM.render(<App/>, document.getElementById("root"))
*/

// import React from "react"
// import ReactDOM from "react-dom"
// import AppPhaseOne6 from "../components/AppPhaseOne6";


// ReactDOM.render(<AppPhaseOne6/>, document.getElementById("root"))

/****12***
import React from "react"
import ReactDOM from "react-dom"
import App11 from "./components/App11"
import "./index.css"


ReactDOM.render(<App11/>, document.getElementById("root"))
*/

/****13***
import React from "react"
import ReactDOM from "react-dom"
import App12 from "./components/App12"
import "./index.css"

ReactDOM.render(<App12/>, document.getElementById("root"))
*/

/****14***
import React from 'react'
import ReactDOM from 'react-dom'

import App13 from "./components/App13"

ReactDOM.render(<App13 />, document.getElementById('root'))
*/