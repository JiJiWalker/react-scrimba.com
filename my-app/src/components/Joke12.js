import React from "react"

function Joke12(props) {

    const style = {}

    if (props.question === "" || props.question == null) {
        style.display = "none";
    } 

    return(
        <div>
            <section className="joke-section">
                {/* could be ternat operator
                    like
                    style={{display: props.question ? "block" : "none"}} 
                    or
                    style={{display: !props.question && "none"}}
                    or in answer with coloer
                    style={{color: !props.question && "grey"}}*/}
                <p className="q" style={style}>Question: {props.question}</p>
                <p className="j">Punch line: {props.punchLine}</p>
            </section>
            <hr/>
        </div>
    )
}

export default Joke12