import React from "react"
import Product13 from "./Product13";
import productData from "./vschoolProducts13"

function App13() {

    const products = productData
        .map(prod => <Product13 key={prod.id} items={prod}/>)

    return (
        <div>
            {products}
        </div>
    )
}

export default App13