import React from "react"
import Footer4 from "./Footer4"
import NavUlLi4 from "./NavUlLi4";
import Main4 from "./Main4";

function AppPractice5() {
    return (
        <div>
            <NavUlLi4/>
            <Main4/>
            <Footer4/>
        </div>
    )
}

export default AppPractice5