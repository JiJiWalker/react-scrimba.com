import React from "react"
import Joke12 from "./Joke12"
import jokes12Data from "./jokes12Data"

function App12() {

    // higher order methods
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findindex
    // const nums = [1, 2, 3, 4, 5];
    // parameter is number from array,
    // map allows to do custom things with elements
    // const doubledNums = nums.map(function(num) {
    //     return num * 2;
    // });

    // jokes12Data.map(function(jokes) {
    //     return <Joke12 question={jokes.question} punchLine={jokes.punchLine}/>
    // })
    const jokeComponent2 = jokes12Data
        .filter(joke => {
            return joke.id % 2 === 0;
        })
        .map(joke => {
            return <Joke12 key={joke.id} question={joke.question} punchLine={joke.punchLine}/>
        })
    // const jokeComponent = jokes12Data.map((joke) => <Joke12 key={joke.id} question={joke.question} punchLine={joke.punchLine}/>)

    return (
        <div>
            {jokeComponent2}
        </div>
    )
}

export default App12