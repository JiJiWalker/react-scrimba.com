import React from "react"

// function App14(props) {
//     return (
//         <div>{props.whatever}</div>
//     )
// }

class App14 extends React.Component {

    someFunc() {

    }

    render() {

        this.someFunc();

        // logic goes here

        return (
            <div>{this.props.whatever}</div>
        )
    }
}

export default App14