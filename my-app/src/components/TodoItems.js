import React from "react"

function TodoItems(props) {

    return (
        <div className="todo-item">
            <input type="checkbox" name="check" id="check" checked={props.items.completed}></input>
            <p for="check">{props.items.text}</p>
        </div>
    )
}

export default TodoItems