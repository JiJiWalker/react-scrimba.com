import React from "react"
import TodoItems from "./TodoItems"
import todoData from "./todoData"

function App() {

    const checkList = todoData
        .map(data => <TodoItems key={data.id} items={data}/>)

    return (
        <div className="todo-list">
            {checkList}}
        </div>
    )
}

export default App