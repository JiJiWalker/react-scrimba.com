import React from "react"

function Product13(props) {

    return (
        <div>
            <p>Name: {props.items.name}</p>
            <p>Price: {props.items.price.toLocaleString("en-US", {style: "currency", currency: "USD"})}</p>
            <p>Description: {props.items.description}</p>
            <hr/>
        </div>
    )
}

export default Product13