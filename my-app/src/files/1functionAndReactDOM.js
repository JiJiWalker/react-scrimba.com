import React from 'react';
import ReactDOM from 'react-dom';

// JSX
// ReactDOM.render(WHAT DO I WANT TO RENDER, WHERE DO I WANT TO RENDER IT)
// ReactDOM.render(<div><h1>Hello Word!</h1><p>This is paragraph</p></div>, document.getElementById("root"));

function MyApp() {
    return (
        <ul>
            <li>First</li>
            <li>Second</li>
            <li>Last</li>
        </ul>
    )
}

ReactDOM.render(
    <MyApp />,
    document.getElementById("root"));