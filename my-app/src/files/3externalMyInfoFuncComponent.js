import React from "react"
import ReactDOM from "react-dom"
import MyInfoFunc from "../components/MyInfoFunc";

ReactDOM.render(
    <MyInfoFunc/>, document.getElementById("root")
)